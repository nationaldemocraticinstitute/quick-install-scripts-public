# README #

Right now, the only description I have is that these are scripts that do a lot of the hard work for you.  In addition, they have been run in a variety of settings to ensure steps have not been skipped.  Any additions made to this library should be considered *public*: NO KEYS, NO LOGINS, NO PASSWORDS, NO PROPRIETARY NDI INFO.