#!/bin/bash

# Quick and Dirty: no Apache or MySQL installed, but needed, obviously to run Drupal
DRUPALVER="7.50"
DOCPATH="/var/www/html"
APACHE_USER="apache"

echo -e "\e[33;1m"
echo "Installing drupal-${DRUPALVER}.tar.gz..."
echo -e "\e[0m"

wget -q https://ftp.drupal.org/files/projects/drupal-${DRUPALVER}.tar.gz -O /tmp/drupal.tar.gz
tar xzf /tmp/drupal.tar.gz -C /tmp/
cp -urp /tmp/drupal*/* ${DOCPATH}/
cp -urp /tmp/drupal*/.htaccess ${DOCPATH}/
mkdir -p ${DOCPATH}/sites/default/files
chmod 755 ${DOCPATH}/sites/default/files
chown -R ${APACHE_USER}:${APACHE_USER} ${DOCPATH}/*
cp ${DOCPATH}/sites/default/default.settings.php ${DOCPATH}/sites/default/settings.php
chown ${APACHE_USER}:${APACHE_USER} ${DOCPATH}/sites/default/settings.php

echo -e "\e[33;1m"
echo "Installing drush..."
echo -e "\e[0m"
php -r "readfile('http://files.drush.org/drush.phar');" > drush
chmod +x drush
mv drush /usr/local/bin
php /usr/local/bin/drush init
drush core-status