#!/bin/bash

# This script creates a Drupal instance on an NDI-owned AWS server.

# Default "no" until I have time to do this elegantly
INSTALL_PHP=0
INSTALL_APACHE=0
INSTALL_MYSQL=0
INSTALL_DRUPAL=0
INSTALL_DRUSH=0

# Some of these for later use /shrug
# PHPVER="56"
# APACHEVER="24"
# MYSQLVER="56"
DRUPALVER="7.50"

TEST_AMI=$(grep "Amazon Linux AMI" /etc/issue)

TMP_MYSQL="/tmp/mysql.sql"
GENPASS_MYSQL_ROOT=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 16 | head -n 1 | tr 1l0Oo xxxxx)
GENPASS_MYSQL_USER=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 16 | head -n 1 | tr 1l0Oo xxxxx)
PASS_FILE="/tmp/generated_passwords.txt"
NDI_DBUSER="ndi_dbuser"
NDI_DATABASE="ndi_database"

function CheckifRoot {

# Must be run as root
if [ "$(whoami)" != 'root' ]; then
        echo -e "\e[31;1m$0: ERROR: You have no permission to run $0 as non-root user.\e[0m"
        exit 1;
fi

}

function TestifAMI {

# You must be on an AWS site, at least for now.
if [[ "${TEST_AMI}" != "" ]]
  then 
    echo -e "\e[37;1mThis is an Amazon instance ${TEST_AMI}\e[0m"
  else 
    echo -e "\e[31;1mThis is not an AWS AMI system: expected \"Amazon Linux AMI\" but got \"${TEST_AMI}\" instead.  Exiting.\e[0m"
    exit 1;
fi

}

function AreYouSure {
# Are you sure?
echo -e "\e[31;1m"
read -p "
**************************************************
Proceeding from here will wipe out existing Apache
or HTTP/HTTPS configs, change the version of PHP, 
Apache, and MySQL. No undo, no gives, no takes!
===================================================

Type \"yes\" to continue, or anything else to quit [quit]: " TEMPYN

if [ "${TEMPYN}" != "yes" ]
then 
  echo "No changes have been made, exiting..."
  echo -e "\e[0m"
  exit 1;
fi
}

function ChooseInstall {

echo -e "\e[33;1m"
echo "
=] What do you want to install? [=
----------------------------------
[1] - Everything (LAMP + Drush)

[2] - PHP 5.6 only
[3] - Apache 2.4 only
[4] - MySQL 5.6 only

[5] - Drupal ${DRUPALVER}, Apache 2.4, and PHP 5.6
[6] - Drush, Apache 2.4, and PHP 5.6 with NO DRUPAL/NO MYSQL
[7] - Drupal ${DRUPALVER}, Drush, Apache 2.4, and PHP 5.6, NO MYSQL

[8] - Just a LAMP stack (Apache, MySQL, PHP)

[0] - (or anything else) I don't like change!!
"
echo -e "\e[0m"
read TEMPYN
    case "${TEMPYN}" in

	    1)
		INSTALL_PHP=1
		INSTALL_APACHE=1
		INSTALL_MYSQL=1
		INSTALL_DRUPAL=1
		INSTALL_DRUSH=1
		;;
	    2)
		INSTALL_PHP=1
		INSTALL_APACHE=0
		INSTALL_MYSQL=0
		INSTALL_DRUPAL=0
		INSTALL_DRUSH=0
		;;
	    3)
		INSTALL_PHP=0
		INSTALL_APACHE=1
		INSTALL_MYSQL=0
		INSTALL_DRUPAL=0
		INSTALL_DRUSH=0
		;;
	    4)
		INSTALL_PHP=0
		INSTALL_APACHE=0
		INSTALL_MYSQL=1
		INSTALL_DRUPAL=0
		INSTALL_DRUSH=0
		;;
	    5)
		INSTALL_PHP=1
		INSTALL_APACHE=1
		INSTALL_MYSQL=0
		INSTALL_DRUPAL=1
		INSTALL_DRUSH=0
		;;
	    6)
		INSTALL_PHP=1
		INSTALL_APACHE=1
		INSTALL_MYSQL=0
		INSTALL_DRUPAL=0
		INSTALL_DRUSH=1
		;;
	    7)
		INSTALL_PHP=1
		INSTALL_APACHE=1
		INSTALL_MYSQL=0
		INSTALL_DRUPAL=1
		INSTALL_DRUSH=1
		;;
	    8)
		INSTALL_PHP=1
		INSTALL_APACHE=1
		INSTALL_MYSQL=1
		INSTALL_DRUPAL=0
		INSTALL_DRUSH=0
		;;
	    *)
		echo "RUN AWAY!!!!"
		exit 1;
		;;

    esac

}

function InstallPHP {
  if [[ ${INSTALL_PHP} == "1" ]]; then 
	echo -e "\e[33;1m"
	echo "Removing any old existing versions of PHP..."
	for foo in $(rpm -qa | grep php); do rpm -e --nodeps $foo; done
	echo "Installing php v5.6..."
	yum -q -y install php56 php56-common php56-process php56-cli php56-xml php56-pdo php56-gd php56-pdo php56-mysqlnd php56-mssql php56-mbstring php-pear
	
	# Note: AWS Ubuntu 14.04 LTS apt-get will also install Apache2, then start it without asking :(
	# apt-get install -y php5 php5-common php5-dev php5-cli php5-mysql php5-gd php5-mssql php-pear libapache2-mod-php5
	# Come on, Ubuntu.  Don't start a service I haven't even set up yet!
	# service apache2 stop
	
	echo -e "\e[0m"
  else 
	echo "- No PHP install requested."
  fi
} 

function InstallApache {
  if [[ ${INSTALL_APACHE} == "1" ]]; then 
	echo -e "\e[33;1m"
	echo "Removing any old existing versions of Apache..."
	for foo in $(rpm -qa | grep httpd); do rpm -e --nodeps $foo; done
	echo "Installing Apache 2.4..."
	yum -q -y install httpd24 httpd24-tools httpd24-manual mod24_ssl
	# Note: AWS Ubuntu 14.04 LTS apt-get takes care of almost all modules including mod_ssl, then run it without asking :(
	# apt-get install -y apache2-common
	echo -e "\e[0m"
	sed -i "s/\#ServerName\ www.example.com\:80/ServerName\ ${HOSTNAME}.ndi.org/" /etc/httpd/conf/httpd.conf
		# Install aws-super-ssl.conf when I make one
		# Settings taken from here: 
		# https://mozilla.github.io/server-side-tls/ssl-config-generator/
		# Install aws-super-httpd.conf when I make one
	chkconfig httpd on
	# Come on, Ubuntu.  Don't start a service I haven't even set up yet!
	# service apache2 stop
  else 
	echo "- No Apache install requested."
  fi
} 

function InstallMySQL {
  if [[ ${INSTALL_MYSQL} == "1" ]]; then 
	echo -e "\e[33;1m"
	echo "Removing any old existing versions of MySQL..."
	for foo in $(rpm -qa | grep ^mysql); do rpm -e --nodeps $foo; done
	echo "Installing MySQL 5.6..."
	yum -q -y install mysql56-server mysql56-common
	# Note: AWS Ubuntu 14.04 LTS mysql-server-5.6 will launch without asking, and then give you 4-5 prompts to set the root password. :(
	# apt-get install -y mysql-server-5.6
	# service mysql stop
	echo -e "\e[0m"
	
	read -p "Please enter MySQL new root password [default: ${GENPASS_MYSQL_ROOT}]: " TEMPYN
	if [ "${TEMPYN}" ]; then GENPASS_MYSQL_ROOT=${TEMPYN:0:15}; fi

	read -p "Please enter Drupal database NAME [default: ${NDI_DATABASE}]: " TEMPYN
	if [ "${TEMPYN}" ]; then NDI_DATABASE=${TEMPYN:0:15}; fi

	read -p "Please enter Drupal database ${NDI_DATABASE} USER [default: ${NDI_DBUSER}]: " TEMPYN
	if [ "${TEMPYN}" ]; then NDI_DBUSER=${TEMPYN:0:15}; fi

	read -p "Please enter ${NDI_DBUSER} user password [default: ${GENPASS_MYSQL_USER}]: " TEMPYN
	if [ "${TEMPYN}" ]; then GENPASS_MYSQL_USER=${TEMPYN:0:15}; fi

	DB_DATA=$(printf "MySQL-Root Pass ${GENPASS_MYSQL_ROOT}\nDatabase Name ${NDI_DATABASE}\n${NDI_DATABASE} User ${NDI_DBUSER}\n${NDI_DBUSER} Pass ${GENPASS_MYSQL_USER}\n")

	echo -e "\e[36;1m"
	echo "Starting and initializing MySQL database.  Please wait..."
	echo -e "\e[0m"
	service mysqld start
	  echo "... waiting 15 seconds"
	  sleep 15;
	chkconfig mysqld on
	echo "UPDATE mysql.user SET Password = PASSWORD('${GENPASS_MYSQL_ROOT}') WHERE User = 'root';" > ${TMP_MYSQL}
	echo "CREATE DATABASE ${NDI_DATABASE};" >> ${TMP_MYSQL}
	echo "CREATE USER ${NDI_DBUSER}@localhost IDENTIFIED BY '${GENPASS_MYSQL_USER}';" >> ${TMP_MYSQL}
	echo "GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, INDEX, ALTER, CREATE TEMPORARY TABLES ON ${NDI_DATABASE}.* TO '${NDI_DBUSER}'@'localhost' IDENTIFIED BY '${GENPASS_MYSQL_USER}';" >> ${TMP_MYSQL}
	echo "FLUSH PRIVILEGES;" >> ${TMP_MYSQL}

	mysql -u root < ${TMP_MYSQL}

	echo -e "\e[37;1m"
	echo "We are done! "

	echo -e "${DB_DATA}\n" | column -t >> ${PASS_FILE}
	echo -e "The following credentials have been made:\n\e[0m"
	echo -e "${DB_DATA}\n" | column -t
	echo -e "\e[35;1m\n This has been saved in ${PASS_FILE} \n which will be deleted upon next reboot.\n PLEASE PUT THIS IN LASTPASS NOW!\n\e[0m"

	echo "To finish installing drupal, turn the Apache web service on."
	echo "Remember to chmod 644 /var/www/html/sites/default/settings.php once Drupal has been set up."
		
  else 
	echo "- No MySQL install requested."
  fi
} 

function InstallDrupal {
  if [[ ${INSTALL_DRUPAL} == "1" ]]; then 
      # Install drupal: note, those jerkwads don't have a "drupal-current" link, so we're going
      #   to have to keep an eye on the version from time to time.  
      echo -e "\e[33;1m"
      echo "Installing drupal-${DRUPALVER}.tar.gz..."
      echo -e "\e[0m"
      wget -q https://ftp.drupal.org/files/projects/drupal-${DRUPALVER}.tar.gz -O /tmp/drupal.tar.gz
      tar xzf /tmp/drupal.tar.gz -C /tmp/
      cp -urp /tmp/drupal*/* /var/www/html/
      cp -urp /tmp/drupal*/.htaccess /var/www/html/
      mkdir -p /var/www/html/sites/default/files
      chmod 755 /var/www/html/sites/default/files
      chown -R apache:apache /var/www/html/*
      cp /var/www/html/sites/default/default.settings.php /var/www/html/sites/default/settings.php
      chown apache:apache /var/www/html/sites/default/settings.php
  else 
	echo "- No Drupal install requested."
  fi
} 

function InstallDrush {
  if [[ ${INSTALL_DRUSH} == "1" ]]; then 
	echo -e "\e[33;1m"
	echo "Installing drush..."
	echo -e "\e[0m"
	php -r "readfile('http://files.drush.org/drush.phar');" > drush
	chmod +x drush
	mv drush /usr/local/bin
	php /usr/local/bin/drush init
	drush core-status
  else 
	echo "- No Drush install requested."
  fi
} 

CheckifRoot
TestifAMI
AreYouSure
ChooseInstall
InstallPHP
InstallApache
InstallMySQL
InstallDrupal
InstallDrush

echo "------"
echo "NOTE: If you installed Apache, you must start it manually (for" 
echo "      safety/security reasons)!  And if you have done the install" 
echo "      for Drupal, please change the file:" 
echo -e "      \e[31;1m/var/www/html/sites/default/settings.php\e[0m permissions"
echo "      back to 644 AFTER you do the web setup (again, security)."
echo -e "\e[0m"
exit 0
